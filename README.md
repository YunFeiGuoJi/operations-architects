## operations-architects 
此项目主要用于运维架构师中涉及到一些的方面，大致分以下模块来概述。
- 架构层面
- 语言能力
- 技能方向
- 团队管理
- 沟通能力

#### 架构层面
主要包含如下几个方面：
1、网络架构
2、存储架构
3、系统架构
4、应用架构
5、性能分析
6、监控体系

#### 技能方向
- 云服务商
    1、腾讯云
    2、阿里云
    3、aws
- 数据库
    1、mysql
    2、redis
    3、mongodb
- linux服务
- 大数据
- 中间件
    1、rabbitmq
    2、zookeeper
    3、nacos
    4、es
    5、skywalking
    6、kafka
    7、consul
- Devops
    1、jenkins
    2、github
    3、dockerhub
    4、kubernetes
    5、ansible
- 负载均衡
    1、nginx
    2、haproxy
    3、slb
    4、f5
- API网关
    1、traefik
    2、openresty
    3、kong

#### 语言能力
- python
- go
- java
- groovy
- 前端

#### 团队管理
分别制定维护标准，规范化，安全性和反馈机制


#### 沟通管理
主要培养与各端之间的沟通，横向沟通和纵向沟通




